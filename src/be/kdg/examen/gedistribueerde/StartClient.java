package be.kdg.examen.gedistribueerde;

import be.kdg.examen.gedistribueerde.client.*;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.ServerStub;


public class StartClient {
    public static void main(String[] args){
        String serverIp = "127.0.0.1";
        int serverPort = 9842;
        DocumentImpl document = new DocumentImpl();
        NetworkAddress contactsAddress = new NetworkAddress(serverIp, serverPort);
        ServerStub contactsStub = new ServerStub(contactsAddress);
        Client client = new Client(contactsStub, document);
        client.run();
    }
}
