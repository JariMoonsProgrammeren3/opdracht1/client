package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class ServerStub implements Server{
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public ServerStub(NetworkAddress serverAddress) {
        this.serverAddress = serverAddress;
        this.messageManager = new MessageManager();
    }

    @Override
    public void log(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "log");
        message.setParameter("document", document.getText());
        messageManager.send(message, serverAddress);
        messageManager.wReceive();
    }

    @Override
    public Document create(String s) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "create");
        message.setParameter("string", s);
        messageManager.send(message,serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        return new DocumentImpl(reply.getParameter("text"));
    }

    @Override
    public void toUpper(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toUpper");
        message.setParameter("document", document.getText());
        messageManager.send(message,serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        document.setText(reply.getParameter("text"));
    }

    @Override
    public void toLower(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toLower");
        message.setParameter("document", document.getText());
        messageManager.send(message,serverAddress);
        MethodCallMessage reply = messageManager.wReceive();
        document.setText(reply.getParameter("text"));
    }

    @Override
    public void type(Document document, String text) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "type");
        for (char character : text.toCharArray()) {
            message.setParameter("document", document.getText());
            message.setParameter("newText", String.valueOf(character));
            messageManager.send(message,serverAddress);
            MethodCallMessage reply = messageManager.wReceive();
            document.setText(reply.getParameter("text"));
        }
    }
}
